#include <iostream>
#include "ev.h"
#include "g.h"

using namespace std;

int main()
{
	Graph *graf = new Graph;

	graf->addVertex(1);
	graf->addVertex(2);
	graf->addVertex(3);
	graf->addVertex(4);
	graf->addVertex(5);
	graf->addEdge(11, 1, 2);
	graf->addEdge(12, 2, 3);
	graf->addEdge(13, 4, 2);
	graf->addEdge(14, 4, 1);
	graf->addEdge(15, 2, 5);
	graf->addEdge(16, 3, 5);
	graf->vertices();
	graf->edges(); 
	graf->incidentEdges(1); 
	graf->addVertex(4);
	graf->replaceV(4, 8);
	graf->vertices(); 
	graf->removeE(1);
	graf->edges();
	graf->removeV(2);
	graf->vertices();
	graf->edges();
	graf->addVertex(15);
	graf->vertices();
	graf->edges();
	graf->removeV(1);
	graf->vertices();
	graf->edges(); 
	graf->addEdge(17, 15, 8);
	graf->vertices();
	graf->edges();
}
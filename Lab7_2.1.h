#pragma once
#include <iostream>

using namespace std;

template <typename Typ>
class wezel {
public:
	Typ wartosc; // wartosc przechowywana przez wezel
	wezel *ojciec; // wskaznik na ojca
	wezel *lSyn; // wskaznik na lewego syna
	wezel *pSyn; // wskaznik na prawego syna
	int LczyP; // 0 gdy jest lewym synem, 1 gdy prawym
	bool czyLisc(); // sprawdza czy wezel jest lisciem
};

template <typename Typ>
bool wezel<Typ>::czyLisc()
{
	if (lSyn == nullptr && pSyn == nullptr)
		return true; // zwraca true gdy nie ma synow
	else
		return false; // zwraca falsz gdy ma co najmniej 1 syna
}
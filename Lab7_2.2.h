#pragma once
#include <iostream>
#include <math.h>
#include "Wezel.h"

using namespace std;

template <typename Typ>
class drzewoBin {
public:
	drzewoBin(); // konstruktor
	~drzewoBin(); // destruktor
	void usun(Typ wartosc); // usuwa wezel o danej wartosci
	void usunDrzewo(); // usuwa cale drzewo
	void dodaj(Typ wartosc); // dodaje wartosc do drzewa
	wezel<Typ>* Korzen(); // zwraca korzen drzewa
	Typ KorzenW(); // zwraca wartosc korzenia
	wezel<Typ>* min(wezel<Typ> *wezel); // szuka najmniejsza wartosc w drzewie
	void preOrder(); // przejscie drzewa wzdluzne
	void postOrder(); // przejscie drzewa wsteczne
	void inOrder(); // przejscie drzewa poprzecznie
	int nodeHeight(wezel<Typ> *wezel); // zwraca wysokosc wezla
	int balanceFactor(wezel<Typ> *wezel); // sprawdza czy drzewo jest zbalansowane
	wezel<Typ>* balance(wezel<Typ> *wezel); // funkcja przywracajaca rownowage dla wezla
	void balanceTree(); // funkcja przywracajaca rownowage w drzewie
	wezel<Typ>* rotateRR(wezel<Typ> *wezel); // rotacja pojedyncza prawej strony
	wezel<Typ>* rotateRL(wezel<Typ> *wezel); // rotacja podwojna prawej strony
	wezel<Typ>* rotateLL(wezel<Typ> *wezel); // rotacja pojedyncza lewej strony
	wezel<Typ>* rotateLR(wezel<Typ> *wezel); // rotacja podwojna lewej strony
	void wyswietl();
private:
	wezel<Typ> *korzen; // wskaznik na korzen drzewa
	void usun(Typ wartosc, wezel<Typ> *lisc); // metoda pomocnicza do usuwania wezla
	void usunDrzewo(wezel<Typ> *lisc); // metoda pomocnicza do usuwania drzewa
	void dodaj(Typ wartosc, wezel<Typ> *lisc); // metoda pomocnicza do dodawania elementu
	void preOrder(wezel<Typ> *wezel); // metoda pomocnicza do przejscia drzewa
	void postOrder(wezel<Typ> *wezel); // metoda pomocnicza do przejscia drzewa
	void inOrder(wezel<Typ> *wezel); // metoda pomocnicza do przejscia drzewa
	void wyswietl(wezel<Typ> *wezel);
};

template <typename Typ>
drzewoBin<Typ>::drzewoBin()
{
	korzen = nullptr;
}

template <typename Typ>
drzewoBin<Typ>::~drzewoBin()
{
	usunDrzewo();
}

template <typename Typ>
wezel<Typ>* drzewoBin<Typ>::Korzen()
{
	return korzen;
}

template <typename Typ>
Typ drzewoBin<Typ>::KorzenW()
{
	return korzen->wartosc;
}

template <typename Typ>
wezel<Typ>* drzewoBin<Typ>::min(wezel<Typ> *wezel)
{
	while (wezel->lSyn != nullptr) {
		wezel = wezel->lSyn;
		min(wezel);
	}
	return wezel;
}

template <typename Typ>
void drzewoBin<Typ>::usun(Typ wartosc, wezel<Typ> *lisc)
{
	if (lisc != nullptr) {
		if (wartosc == lisc->wartosc) { // znalezienie wezla do usuniecia
			if (lisc != Korzen() || (lisc == Korzen() && Korzen()->lSyn != nullptr && Korzen()->pSyn != nullptr)) {
				if (lisc->czyLisc()) { // gdy usuwany wezel jest lisciem
					if (lisc->LczyP == 0) // ojciec traci syna
						lisc->ojciec->lSyn = nullptr;
					else if (lisc->LczyP == 1)
						lisc->ojciec->pSyn = nullptr;
				}
				else if (lisc->lSyn != nullptr && lisc->pSyn != nullptr) { // gdy wezel ma 2 synow
					wezel<Typ> *nowy = min(lisc->pSyn); // znalezienie minimum z prawej strony wezla do usuniecia
					int temp = lisc->wartosc; // zamiana miejscami dwoch wartosci
					lisc->wartosc = nowy->wartosc;
					nowy->wartosc = temp;
					usun(wartosc, lisc->pSyn); // usuniecie danej wartosci juz z innego miejsca wezla
				}
				else { // gdy wezel ma jednego syna
					if (lisc->lSyn != nullptr) { // sprawdzenie czy ma lewego czy prawego syna
						lisc->lSyn->ojciec = lisc->ojciec; // nowym ojcem syna bedzie jego dziadek
						if (lisc->LczyP == 0)
							lisc->ojciec->lSyn = lisc->lSyn; // nowym synem dziadka bedzie jego wnuk
						else if (lisc->LczyP == 1)
							lisc->ojciec->pSyn = lisc->lSyn;
					}
					else {
						lisc->pSyn->ojciec = lisc->ojciec; // nowym ojcem syna bedzie jego dziadek
						if (lisc->LczyP == 0)
							lisc->ojciec->lSyn = lisc->pSyn; // nowym synem dziadka bedzie jego wnuk
						else if (lisc->LczyP == 1)
							lisc->ojciec->pSyn = lisc->pSyn;
					}
				}
			}
			else { // gdy do usuniecia jest korzen z 0 lub 1 synem
				if (Korzen()->lSyn == nullptr && Korzen()->pSyn == nullptr) // gdy korzen nie ma synow
					usunDrzewo();
				else if (Korzen()->lSyn != nullptr) { // gdy korzen ma tylko lewego syna
					korzen->lSyn->ojciec = nullptr;
					korzen = korzen->lSyn;
				}
				else { // gdy korzen ma tylko prawego syna
					korzen->pSyn->ojciec = nullptr;
					korzen = korzen->pSyn;
				}
			}
		}
		else if (wartosc < lisc->wartosc)
			usun(wartosc, lisc->lSyn); // wywolanie funkcji dla lewego syna
		else
			usun(wartosc, lisc->pSyn); // wywolanie funkcji dla prawego syna
	}
	else
		cout << "Nie ma takiego elementu w drzewie!\n\n";
}

template <typename Typ>
void drzewoBin<Typ>::usun(Typ wartosc)
{
	cout << "Usuwanie liczby: " << wartosc << endl;
	usun(wartosc, Korzen());
	balanceTree();
}

template <typename Typ>
void drzewoBin<Typ>::usunDrzewo(wezel<Typ> *lisc)
{
	if (lisc != nullptr) { // sprawdza czy wezel jest lisciem
		usunDrzewo(lisc->lSyn); // dojscie do lisci drzewa
		usunDrzewo(lisc->pSyn);
		delete lisc;
	}
}

template <typename Typ>
void drzewoBin<Typ>::usunDrzewo()
{
	usunDrzewo(korzen);
}

template <typename Typ>
void drzewoBin<Typ>::dodaj(Typ wartosc, wezel<Typ> *lisc)
{
	wezel<Typ> *nowy = new wezel<Typ>;
	if (wartosc < lisc->wartosc) { // sprawdza czy isc w lewo czy w prawo
		if (lisc->lSyn != nullptr) // sprawdza czy wezel ma synow
			dodaj(wartosc, lisc->lSyn); // zejscie do kolejnego wezla
		else {
			nowy->wartosc = wartosc;
			nowy->LczyP = 0; // wezel jest lewym synem
			lisc->lSyn = nowy; // dodanie nowego elementu
			lisc->lSyn->ojciec = lisc; // ustawienie ojca nowego elementu
			lisc->lSyn->lSyn = nullptr; // ustawienie synow nowego wezla na null
			lisc->lSyn->pSyn = nullptr;
		}
	}
	else if (wartosc > lisc->wartosc) { // gdy wartosc dodawana wieksza niz wartosc wezla
		if (lisc->pSyn != nullptr)
			dodaj(wartosc, lisc->pSyn); // zejscie do kolejnego wezla
		else {
			nowy->wartosc = wartosc;
			nowy->LczyP = 1; // wezel jest prawym synem
			lisc->pSyn = nowy; // dodanie nowego elementu
			lisc->pSyn->ojciec = lisc; // ustawienie ojca nowego elementu
			lisc->pSyn->lSyn = nullptr; // ustawienie synow nowego wezla na null
			lisc->pSyn->pSyn = nullptr;
		}
	}
	else
		cout << "Wartosc " << wartosc << " juz znajduje sie w drzewie!\n";
}

template <typename Typ>
void drzewoBin<Typ>::dodaj(Typ wartosc)
{
	cout << "Dodawanie liczby: " << wartosc << endl;
	if (korzen != nullptr) // gdy drzewo ma juz korzen
		dodaj(wartosc, korzen);
	else { // gdy drzewo nie ma korzenia
		wezel<Typ> *nowy = new wezel<Typ>;
		nowy->wartosc = wartosc;
		korzen = nowy; // ustawienie nowej wartosci jako korzen
		korzen->lSyn = nullptr;
		korzen->pSyn = nullptr;
	}
	balanceTree(); // balansowanie drzewa po dodaniu elementu
}

template <typename Typ>
void drzewoBin<Typ>::preOrder(wezel<Typ> *wezel)
{ // wezel odwiedzany przed jego potomkami
	cout << wezel->wartosc << " "; // wyswietlenie wartosci elementu
	if (wezel->lSyn != nullptr)
		preOrder(wezel->lSyn);
	if (wezel->pSyn != nullptr)
		preOrder(wezel->pSyn);
	//if (wezel->czyLisc())
	//cout << endl; // od nowej linii gdy natrafiono na lisc
}

template <typename Typ>
void drzewoBin<Typ>::preOrder()
{
	preOrder(Korzen());
}

template <typename Typ>
void drzewoBin<Typ>::postOrder(wezel<Typ> *wezel)
{ // wezel jest odwiedzany po jego potomkach
	if (wezel->lSyn != nullptr)
		postOrder(wezel->lSyn);
	if (wezel->pSyn != nullptr)
		postOrder(wezel->pSyn);
	cout << wezel->wartosc << " "; // wyswietlenie wartosci elementu
}

template <typename Typ>
void drzewoBin<Typ>::postOrder()
{
	postOrder(Korzen());
}

template <typename Typ>
void drzewoBin<Typ>::inOrder(wezel<Typ> *wezel)
{ // wezel odwiedzany po jego lewym poddrzewie i przed prawym
	if (wezel->lSyn != nullptr)
		inOrder(wezel->lSyn);
	cout << wezel->wartosc << " "; // wyswietlenie wartosci elementu
	if (wezel->pSyn != nullptr)
		inOrder(wezel->pSyn);
}

template <typename Typ>
void drzewoBin<Typ>::inOrder()
{
	inOrder(Korzen());
}

// ***************************************************************************************

template <typename Typ>
int drzewoBin<Typ>::nodeHeight(wezel<Typ> *wezel)
{
	int heightLeft = 0;
	int heightRight = 0;

	if (wezel->lSyn)
		heightLeft = nodeHeight(wezel->lSyn); // wywolanie rekurencyjne dla lewego syna
	if (wezel->pSyn)
		heightRight = nodeHeight(wezel->pSyn); // wywolanie rekurencyjne dla prawego syna

	return heightRight > heightLeft ? ++heightRight : ++heightLeft; // zwraca wieksza wysokosc
}

template <typename Typ>
int drzewoBin<Typ>::balanceFactor(wezel<Typ> *wezel)
{
	int bf = 0; // zmienna przechowujaca roznice wysokosci

	if (wezel->lSyn)
		bf += nodeHeight(wezel->lSyn); // wysokosc lewego poddrzewa jest dodawana
	if (wezel->pSyn)
		bf -= nodeHeight(wezel->pSyn); // wysokosc prawego poddrzewa jest odejmowana

	return bf; 
}

template <typename Typ>
wezel<Typ>* drzewoBin<Typ>::balance(wezel<Typ> *Wezel)
{
	wezel<Typ> *nowyKorzen = new wezel<Typ>; 
	wezel<Typ> *temp = new wezel<Typ>;
	
	// wywolanie rekurencyjne funkcji dla synow
	if (Wezel->lSyn != nullptr)
		Wezel->lSyn = balance(Wezel->lSyn);
	if (Wezel->pSyn != nullptr)
		Wezel->pSyn = balance(Wezel->pSyn); 

	int bf = balanceFactor(Wezel); // roznica wysokosci dla danego wezla
	if (bf >= 2) { // lewa strona ma wiecej wezlow
		if (Wezel->lSyn != nullptr && balanceFactor(Wezel->lSyn) <= -1) {
			temp = Wezel->ojciec; // zapamietanie kto byl ojciem gornego wezla
			cout << "LP dla: " << Wezel->wartosc << endl; // rotacja podwojna dla prawej strony
			nowyKorzen = rotateLR(Wezel);
			nowyKorzen->ojciec = temp; // ustawienie ojca nowemu gornemu wezlowi
		}
		else {
			temp = Wezel->ojciec; // zapamietanie kto byl ojciem gornego wezla
			cout << "LL dla: " << Wezel->wartosc << endl; // rotacja pojedyncza dla lewej strony
			nowyKorzen = rotateLL(Wezel);
			nowyKorzen->ojciec = temp; // ustawienie ojca nowemu gornemu wezlowi
		}
	}
	else if (bf <= -2) { // prawa strona ma wiecej wezlow
		if (Wezel->pSyn != nullptr && balanceFactor(Wezel->pSyn) >= 1) {
			temp = Wezel->ojciec; // zapamietanie kto byl ojciem gornego wezla
			cout << "PL dla: " << Wezel->wartosc << endl;
			nowyKorzen = rotateRL(Wezel); // rotacja podwojna dla prawej strony
			nowyKorzen->ojciec = temp; // ustawienie ojca nowemu gornemu wezlowi
		}
		else {
			temp = Wezel->ojciec; // zapamietanie kto byl ojciem gornego wezla
			cout << "PP dla: " << Wezel->wartosc << endl;
			nowyKorzen = rotateRR(Wezel); // rotacja pojedyncza dla prawej strony
			nowyKorzen->ojciec = temp; // ustawienie ojca nowemu gornemu wezlowi
		}
	}
	else {// drzewo jest zbalansowane
		//cout << "OK dla: " << Wezel->wartosc << endl;
		nowyKorzen = Wezel; // korzen sie nie zmienia
	}
	return nowyKorzen;
}

template <typename Typ>
void drzewoBin<Typ>::balanceTree()
{
	wezel<Typ> *nowyKorzen = balance(Korzen()); // wywolywana funkcja balansujaca

	if (nowyKorzen != Korzen()) { // jesli zmienil sie gorny element drzewa to ustawiany jest jako nowy korzen
		korzen = nowyKorzen;
	}
}
template <typename Typ>
wezel<Typ>* drzewoBin<Typ>::rotateRR(wezel<Typ> *Wezel)
{
	wezel<Typ> *temp = Wezel; // wezel ktory jest poczatkowo na gorze
	wezel<Typ> *temp2 = Wezel->pSyn; // prawy syn wezla temp

	temp->pSyn = temp2->lSyn; // gorny wezel przejmuje syna od swojego prawego syna
	if (temp2->lSyn != nullptr)
		temp2->lSyn->ojciec = temp;
	temp2->lSyn = temp; // prawy syn wezla temp idzie na gore
	temp->ojciec = temp2;

	temp2->LczyP = temp->LczyP;
	temp->LczyP = 0;
	if (temp->pSyn != nullptr)
		temp->pSyn->LczyP = 1;
	return temp2; // zwracany gorny element poddrzewa
}

template <typename Typ>
wezel<Typ>* drzewoBin<Typ>::rotateRL(wezel<Typ> *Wezel)
{
	wezel<Typ> *temp = Wezel; // wezel ktory jest poczatkowo na gorze
	wezel<Typ> *temp2 = Wezel->pSyn; // wezel ktory jest poczatkowo drugi
	wezel<Typ> *temp3 = Wezel->pSyn->lSyn; // wezel ktory jest poczatkowo na dole

	temp->pSyn = temp3->lSyn; // wezel pierwszy i drugi przejmuja synow od wezla trzeciego
	if (temp3->lSyn != nullptr)
		temp3->lSyn->ojciec = temp;
	temp2->lSyn = temp3->pSyn;
	if (temp3->pSyn != nullptr)
		temp3->pSyn->ojciec = temp2;
	temp3->lSyn = temp; // wezel ktory byl na dole idzie teraz na gore
	temp->ojciec = temp3;
	temp3->pSyn = temp2;
	temp2->ojciec = temp3;

	temp3->LczyP = temp->LczyP;
	temp->LczyP = 0;
	if (temp->pSyn != nullptr)
		temp->pSyn->LczyP = 1;
	if (temp2->lSyn != nullptr)
		temp2->lSyn->LczyP = 0;
	return temp3; // zwracany gorny element poddrzewa
}

template <typename Typ>
wezel<Typ>* drzewoBin<Typ>::rotateLL(wezel<Typ> *Wezel)
{
	wezel<Typ> *temp = Wezel; // wezel ktory jest poczatkowo na gorze
	wezel<Typ> *temp2 = Wezel->lSyn; // lewy syn wezla temp

	temp->lSyn = temp2->pSyn; // gorny wezel przejmuje syna od swojego lewego syna
	if (temp2->pSyn != nullptr)
		temp2->pSyn->ojciec = temp;
	temp2->pSyn = temp; // lewy syn wezla temp idzie teraz na gore
	temp->ojciec = temp2;

	temp2->LczyP = temp->LczyP;
	temp->LczyP = 1;
	if (temp->lSyn != nullptr)
		temp->lSyn->LczyP = 0;

	return temp2; // zwracany gorny element poddrzewa
}

template <typename Typ>
wezel<Typ>* drzewoBin<Typ>::rotateLR(wezel<Typ> *Wezel)
{
	wezel<Typ> *temp = Wezel; // wezel ktory jest poczatkowo na gorze
	wezel<Typ> *temp2 = Wezel->lSyn; // wezel ktory jest poczatkowo drugi
	wezel<Typ> *temp3 = Wezel->lSyn->pSyn; // wezel ktory jest poczatkowo na dole

	temp->lSyn = temp3->pSyn; // wezel pierwszy i drugi przejmuja synow od wezla trzeciego
	if (temp3->pSyn != nullptr)
		temp3->pSyn->ojciec = temp;
	temp2->pSyn = temp3->lSyn;
	if (temp3->lSyn != nullptr)
		temp3->lSyn->ojciec = temp2;
	temp3->pSyn = temp; // wezel ktory byl na dole idzie teraz na gore
	temp->ojciec = temp3;
	temp3->lSyn = temp2;
	temp2->ojciec = temp3;

	temp3->LczyP = temp->LczyP;
	temp->LczyP = 1;
	if(temp->pSyn!=nullptr)
		temp->pSyn->LczyP = 1;
	if (temp2->lSyn != nullptr)
		temp2->lSyn->LczyP = 0;
	return temp3; // zwracany gorny element poddrzewa
}

template <typename Typ>
void drzewoBin<Typ>::wyswietl()
{
	cout << "Wysokosc drzewa: " << nodeHeight(Korzen()); // wyswietlenie wysokosci drzewa
	cout << "\nWartosc korzenia: " << KorzenW() << endl; // wyswietlenie wartosci korzenia
	if (korzen->lSyn != nullptr) // wyswietlenie lewego poddrzewa
		wyswietl(korzen->lSyn);
	if (korzen->pSyn != nullptr) // wyswietlenie prawego poddrzewa
		wyswietl(korzen->pSyn);
	cout << endl;
}

template <typename Typ>
void drzewoBin<Typ>::wyswietl(wezel<Typ> *wezel)
{
	if (wezel->LczyP == 0)
		cout << "Lewy syn ";
	else
		cout << "Prawy syn ";
	cout << wezel->ojciec->wartosc << " to " << wezel->wartosc << "\n"; // wyswietlenie wartosci elementu
	if (wezel->lSyn != nullptr) // wyswietlanie na zasadzie przejscia preorder
		wyswietl(wezel->lSyn);
	if (wezel->pSyn != nullptr)
		wyswietl(wezel->pSyn);
}

// **************************************************************************************

// funkcja wybierajaca "gorszy" przypadek
int choosebf(int bf1, int bf2)
{
	if (bf1 <= -2 || bf1 >= 2)
		return bf1;
	else
		return bf2;
}

// funkcja zwracajaca wieksza wartosc z dwoch
template <typename Typ>
Typ max(Typ a, Typ b)
{
	if (a >= b)
		return a;
	else
		return b;
}

// funkcja zwracajaca wysokosc drzewa
template <typename Typ>
int wysokosc(drzewoBin<Typ> *tree, wezel<Typ> *node)
{
	if (node->czyLisc()) // sprawdzenie czy wezel jest lisciem
		return 0;
	else {
		int h = 0; // inicjalizacja zmiennej przechowujacej wysokosc drzewa
		if (node->lSyn != nullptr) {
			h = max(h, wysokosc(tree, node->lSyn)); // wywolanie rekurencyjne dla lewego syna
		}
		if (node->pSyn != nullptr) {
			h = max(h, wysokosc(tree, node->pSyn)); // wywolanie rekurencyjne dla prawego syna
		}
		return 1 + h; // zwiekszenie zmiennej przechowujacej wysokosc o 1
	}
}

// funkcja zwracajaca poziom wezla
template <typename Typ>
int poziomWezla(drzewoBin<Typ> *tree, wezel<Typ> *node)
{
	if (node == tree->Korzen())
		return 0;
	return (1 + poziomWezla(tree, node->ojciec)); // wywolanie rekurencyjne dla ojca wezla i zwiekszenie poziomu o 1
}
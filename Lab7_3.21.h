#pragma once
#include <iostream>
#include <vector>

using namespace std;

class Vertex;

class Edge {
public:
	Edge(int waga, Vertex *one, Vertex *two) // konstruktor
	{
		weight = waga; // nadanie wagi
		first = one; // nadanie wierzcholkow ktore laczy dana krawedz
		last = two;
	}
	int weight; // waga krawedzi
	Vertex *first; // jeden koniec krawedzi
	Vertex *last; // drugi koniec krawedzi
};

class Vertex {
public:
	Vertex(int klucz) // konstruktor
	{
		key = klucz;
	}
	int key; // wartosc wierzcholka
	int indeks; // indeks macierzy sasiedztwa powiazany z wierzcholkiem
};
#include <iostream>
#include "Wezel.h"
#include "DrzewoBin.h"

using namespace std;

int main()
{
	drzewoBin<int> *tree = new drzewoBin<int>;
	
	tree->dodaj(7);
	tree->dodaj(3);
	tree->dodaj(10);
	tree->dodaj(1);
	tree->dodaj(5);
	tree->dodaj(12);
	tree->dodaj(8);
	tree->dodaj(13);
	tree->wyswietl();

	cout << "Rotacja PP: \n";
	tree->dodaj(16);
	tree->wyswietl();

	cout << "Rotacja PL: \n";
	tree->dodaj(11);
	tree->wyswietl();

	cout << "Rotacja LP: \n";
	tree->dodaj(-1);
	tree->dodaj(0);
	tree->wyswietl();

	cout << "Rotacja LL: \n";
	tree->dodaj(-3);
	tree->wyswietl();

	cout << "Usuwanie: \n";
	tree->usun(-1);
	tree->usun(-3);
	tree->wyswietl();

	cout << "Usuwanie ze zmiana korzenia: \n";
	tree->usun(1);
	tree->usun(5);
	tree->usun(0);
	tree->wyswietl();
}
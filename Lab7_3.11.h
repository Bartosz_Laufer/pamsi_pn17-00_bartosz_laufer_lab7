#pragma once
#include <iostream>
#include <vector>

using namespace std;

class Vertex;

class Edge {
public:
	Edge(int waga, Vertex *one, Vertex *two) // konstruktor
	{
		weight = waga; // nadanie wagi
		first = one; // nadanie wierzcholkow ktore laczy dana krawedz
		last = two;
	}
	int weight; // waga krawedzi
	Vertex *first; // jeden koniec krawedzi
	Vertex *last; // drugi koniec krawedzi
	Edge* posF; // wskaznik na pozycje krawedzi na liscie jednego wierzcholka
	Edge* posL; // wskaznik na pozycje krawedzi na liscie drugiego wierzcholka
	Edge* pos; // wskaznik na pozycje krawedzi na liscie
};

class Vertex {
public:
	Vertex(int klucz) // konstruktor
	{
		key = klucz;
	}
	int key; // wartosc wierzcholka
	vector<Edge*> adjList2; // lista sasiedztwa danego wierzcholka
	Vertex* pos; // wskaznik na pozycje wierzcholka na liscie
};


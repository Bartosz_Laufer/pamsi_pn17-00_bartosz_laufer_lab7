#pragma once
#include <iostream>
#include "EV.h"

using namespace std;

class Graph {
public:
	Graph();
	vector<Vertex*> vertexList; // tablica wskaznikow na wierzcholki
	Edge* **adjMatrix; // macierz sasiedztwa
	int size; // rozmiar macierzy sasiedztwa
	int numberV; // ilosc utworzonych wierzcholkow
	void addVertex(int klucz); //dodawanie wierzcholka
	void addEdge(int waga, int key1, int key2); // dodawanie krawedzi
	int findVertex(int klucz); // szukanie wierzcholka
	void vertices(); // wyswietlenie wszystkich wierzcholkow w grafie
	void edges(); // wyswietlenie wszystkich krawedzi w grafie
	void incidentEdges(int klucz); // wyswietlenie wszystkich krawedzi incydentnych danego wierzcholka
	void endVertices(Edge *E1); // wyswietlenie wierzcholkow ktore laczy dana krawedz
	Vertex* opposite(int klucz, Edge *E1); // zwrocenie wierzcholka przeciwleglego wzgledem danej krawedzi
	void replaceV(int staryklucz, int nowyKlucz); // zastapienie elementu na wierzcholku innym
	void removeE(int waga); // usuwanie krawedzi 
	void removeV(int klucz); // usuwanie wierzcholka i jego krawedzi incydentnych

};

Graph::Graph()
{
	adjMatrix = new Edge* *[5]; // alokacja pamieci
	for (int i = 0; i < 5; i++) {
		adjMatrix[i] = new Edge*[5]; // alokacja pamieci
		for (int j = 0; j < 5; j++)
			adjMatrix[i][j] = nullptr; // wypelnienie tablicy nullptrami
	}
	numberV = 0;
	size = 5;
}

void Graph::addVertex(int klucz)
{
	if (findVertex(klucz) == -1) { // gdy nie ma takiego wierzcholka w grafie
		Vertex *V1 = new Vertex(klucz); // tworzenie nowego wierzcholka
		vertexList.push_back(V1); // dodanie wierzcholka do listy
		if (numberV + 1 == size) { // sprawdzenie czy macierz jest pelna
			size = size * 2; // zwiekszenie rozmiaru tablicy
			Edge* **arrayTemp = new Edge* *[size]; // utworzenie nowej tablicy
			for (int i = 0; i < size; i++) {
				arrayTemp[i] = new Edge*[size]; // alokacja pamieci
				for (int j = 0; j < size; j++)
					arrayTemp[i][j] = nullptr; // wypelnienie tablicy nullptrami
			}
			for (int i = 0; i < size/2; i++) {
				for (int j = 0; j < size/2; j++)
					arrayTemp[i][j] = adjMatrix[i][j]; // wypelnienie tablicy 
			}
			adjMatrix = arrayTemp;
		}
		V1->indeks = numberV;
		numberV++;
		cout << "Dodano wierzcholek o wartosci " << klucz << endl;
	}
	else
		cout << "W grafie jest juz wierzcholek o takiej wartosci!\n";
}

int Graph::findVertex(int klucz)
{
	for (int i = 0; i < vertexList.size(); i++) {
		if (vertexList[i]->key == klucz)
			return i; // zwraca indeks wierzcholka na liscie wierzcholkow
	}
	return -1; // gdy takiego wierzcholka nie ma w grafie
}

void Graph::addEdge(int waga, int key1, int key2)
{
	if (findVertex(key1) == -1 || findVertex(key2) == -1) // sprawdzenie czy podane wierzcholki istnieja
		cout << "Nie mozna dodac krawedzi (co najmniej jeden z podanych wierzcholkow nie istnieje)!\n";
	else {
		Edge *E1 = new Edge(waga, vertexList[findVertex(key1)], vertexList[findVertex(key2)]); // utworzenie krawedzi
		adjMatrix[vertexList[findVertex(key1)]->indeks][vertexList[findVertex(key2)]->indeks] = E1; // dodanie do macierzy wskaznika na krawedz
		adjMatrix[vertexList[findVertex(key2)]->indeks][vertexList[findVertex(key1)]->indeks] = E1;
		cout << "Do wierzcholkow " << key1 << " i " << key2 << " dodano krawedz o wartosci " << waga << endl;
	}
}

void Graph::vertices()
{
	if (vertexList.size() != 0) { // sprawdzenie czy graf ma jakiekolwiek wierzcholki
		cout << endl << "Wszystkie wierzcholki w grafie:\n";
		for (int i = 0; i < vertexList.size(); i++)
			cout << vertexList[i]->key << " "; // wyswietlanie kolejnych wierzcholkow
		cout << endl;
	}
	else
		cout << "Nie ma zadnych wierzcholkow w grafie!\n";
}

void Graph::edges()
{
	if (vertexList.size() == 0) { // sprawdzenie czy graf nie jest pusty
		cout << "Graf jest pusty!\n";
	}
	else {
		cout << endl << "Wszystkie krawedzie w grafie:\n";
		for (int i = 0; i < vertexList.size(); i++) { // petla przechodzaca po wierzcholkach
			cout << "Krawedzie incydentne do wierzcholka " << vertexList[i]->key << ": ";
			for (int j = 0; j < vertexList.size(); j++) {// petla przechodzaca po krawedziach
				if (adjMatrix[i][j] != nullptr)
					cout << adjMatrix[i][j]->weight << " "; // wyswietlanie kolejnych krawedzi
			}
			cout << endl;
		}
	}
}

void Graph::incidentEdges(int key)
{
	int indeks = findVertex(key); // indeks danego wierzcholka
	cout << endl;
	if (indeks != -1) {
		cout << "Krawedzie incydentne do wierzcholka " << vertexList[indeks]->key << ": ";
		for (int i = 0; i < vertexList.size(); i++) {// petla przechodzaca po krawedziach
			if (adjMatrix[vertexList[indeks]->indeks][i] != nullptr)
				cout << adjMatrix[vertexList[indeks]->indeks][i]->weight << " "; // wyswietlanie kolejnych krawedzi
		}
		cout << endl;
	}
	else
		cout << "Podany wierzcholek nie istnieje!\n";
}

void Graph::endVertices(Edge *E1)
{
	if (E1 != nullptr) // sprawdzenie czy dana krawedz istnieje
		cout << "Krawedz o wadze " << E1->weight << " laczy wierzcholki " << E1->first->key << " i " << E1->last->key << endl;
	else
		cout << "Podana krawedz nie istnieje!\n";
}

Vertex* Graph::opposite(int klucz, Edge *E1)
{
	int indeks = findVertex(klucz);
	if (indeks == -1) // sprawdzenie czy podany wierzcholek istnieje
		cout << "Podany wierzcholek nie istnieje!\n";
	else {
		for (int i = 0; i < vertexList.size(); i++) {
			if (adjMatrix[indeks][i] == E1) { // sprawdzenie czy podany wierzcholek posiada dana krawedz
				if (E1->first->key == klucz) // sprawdzenie ktory koniec krawedzi zostal podany
					return vertexList[E1->last->indeks]; // zwrocenie wskaznika do danego wierzcholka
				else
					return vertexList[E1->first->indeks];
			}
		}
		cout << "Podany wierzcholek nie posiada takiej krawedzi!\n";
	}
	Vertex *V1 = new Vertex(-1); // wierzcholek zwracany gdy podane zostaly nie odpowiednie dane
	return V1;
}

void Graph::replaceV(int staryKlucz, int nowyKlucz)
{
	int indeks = findVertex(staryKlucz);
	if (indeks == -1) // sprawdzenie czy dany wierzcholek istnieje
		cout << "Podany wierzcholek nie istnieje!\n";
	else {
		vertexList[indeks]->key = nowyKlucz;
		cout << "Nowa wartosc wierzcholka " << staryKlucz << " to " << nowyKlucz << endl;
	}
}

void Graph::removeE(int waga)
{
	int usunieto = 0; // zwiekszany o 1 gdy element zostal usuniety
	if (vertexList.size() == 0) { // sprawdzenie czy graf nie jest pusty
		cout << "Graf jest pusty!\n";
	}
	else {
		for (int i = 0; i < vertexList.size(); i++) { // petla przechodzaca po wierzcholkach
			for (int j = 0; j < vertexList.size(); j++) {// petla przechodzaca po krawedziach
				if (adjMatrix[i][j] != nullptr) { // znalezienie i usuniecie krawedzi
					if (adjMatrix[i][j]->weight == waga) {
						adjMatrix[i][j] = nullptr;
						usunieto++;
					}
				}
			}
		}
		if (usunieto == 0) // sprawdzenie czy krawedz zostala usunieta (czy istniala krawedz o podanej wadze)
			cout << "Podana krawedz nie istnieje!\n";
		else
			cout << "Krawedz o wartosci " << waga << " zostala usunieta\n";
	}
}

void Graph::removeV(int klucz)
{
	int indeks = findVertex(klucz);
	if (indeks == -1) // sprawdzenie czy dany wierzcholek istnieje
		cout << "Podany wierzcholek nie istnieje!\n";
	else {
		for (int i = 0; i < vertexList.size(); i++) {
			if (adjMatrix[indeks][i] != nullptr) // usuniecie wszystkich krawedzi incydentnych danego wierzcholka
				removeE(adjMatrix[indeks][i]->weight);
		}
		// rekonstrukcja macierzy
		for (int i = 0; i < vertexList.size(); i++) {
			for (int j = 0; j < vertexList.size(); j++) {
				if (i > indeks && j > indeks) {
					adjMatrix[i - 1][j - 1] = adjMatrix[i][j];
					adjMatrix[i][j] = nullptr;
				}
				else if (i > indeks) {
					adjMatrix[i - 1][j] = adjMatrix[i][j];
					adjMatrix[i][j] = nullptr;
				}
				else if (j > indeks) {
					adjMatrix[i][j - 1] = adjMatrix[i][j];
					adjMatrix[i][j] = nullptr;
				}
			}
		}
		vertexList.erase(vertexList.begin() + indeks); // usuniecie danego wierzcholka z listy wierzcholkow
		for (int i = indeks; i < vertexList.size(); i++)
			vertexList[i]->indeks--; // uaktualnienie indeksow wierzcholkow
		numberV--;
		cout << "Wierzcholek o wartosci " << klucz << " zostal usuniety\n";
	} 
}